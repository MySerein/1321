let myall = (arr) => {
  let results = [];  
  let promiseCount = 0;  
  let promisesLength = arr.length;
  return new Promise((resolve, reject) => {
    for (let i = 0; i < arr.length; i++) {
      arr[i].then(res => {
        promiseCount++;
        results[i] = {
          state: "1001",
          data: res
        };
        if (promiseCount === promisesLength) {
          return resolve(results);
        }
      }).catch((err) => { 
        promiseCount++;
        results[i] = {
          state: "1002",
          data: err
        };
        if (promiseCount === promisesLength) {
          return resolve(results);
        }
      })
    }
  });
};

let pro1 = new Promise(resolve => {
  resolve("第一个")
})

let pro2 = new Promise(resolve => {
  resolve("第二个")
})

let pro3 = new Promise((resolve, reject) => {
  reject("第三个")
})

myall([pro1, pro2, pro3]).then(res => {
  console.log(res, "========");
})
