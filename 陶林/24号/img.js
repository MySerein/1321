
let imgList = [...document.querySelectorAll('img')]
const imgLazyLoad = function () {
  let H = window.innerHeight  
  let S = document.documentElement.scrollTop  
  for (let i = 0; i < imgList.length; i++) {
    if ((H + S) > imgList[i].offsetTop) {
      imgList[i].src = imgList[i].getAttribute("data-src")
      imgList.splice(i) 
    }
  }
  if (imgList.length === 0) {
    document.removeEventListener(imgLazyLoad)  
  }
}

document.addEventListener('scroll', imgLazyLoad)