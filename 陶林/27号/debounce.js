function debounce(func, wait, immediate) {
  var timer = null;  //初始化timer，作为计时清除依据
  return function() {
    var context = this;  //获取函数所在作用域this
    var args = arguments;  //取得传入参数
    clearTimeout(timer);
    if(immediate && timer === null) {
        //时间间隔外立即执行
        func.apply(context,args);
      timer = 0;
      return;
    }
    timer = setTimeout(function() {
      func.apply(context,args);
      timer = null;
    }, wait);
  }
}