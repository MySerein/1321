// 当调用 bind 函数后，bind 函数的第一个参数就是原函数作用域中 this 指向的值。
Function.prototype.myBind = function () {
  let self = this 
  context = [].shift.call(arguments)  
  args = [].slice.call(arguments); 
  return function () {  
    return self.apply(context, [].concat.call(args, [].slice.call(arguments)))
  }
}