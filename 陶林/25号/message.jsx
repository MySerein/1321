import {Component} from "react"
class MessageCom extends Component {
  render() {
    return (
      <span style={{ color: this.props.color }}>
        {this.props.text}
      </span>
    );
  }
}

export default function (obj) {
  const { type, text } = obj;

  if (type === "success") {
    return <MessageCom text={text} color="green" />;
  } else if (type === "fail") {
    return <MessageCom text={text} color="red" />;
  }
}
